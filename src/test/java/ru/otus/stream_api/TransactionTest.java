package ru.otus.stream_api;

import static java.sql.Connection.TRANSACTION_READ_UNCOMMITTED;
import static java.sql.Connection.TRANSACTION_REPEATABLE_READ;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.ArrayList;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.postgresql.util.PSQLException;

class TransactionTest extends CommonTest {


    @Test
    @SneakyThrows
    void crudPreparedStatements_AutocommitEnabled_IntermediateStateIsPersisted() {
        Connection externalConnection = getConnection();
        Connection connection = getConnection();
        try (
            PreparedStatement cratePreparedStatement = getCratePreparedStatement(connection);
            PreparedStatement readPreparedStatement = getReadPreparedStatement(externalConnection);
            PreparedStatement deletePreparedStatement = getDeletePreparedStatement(connection);
        ) {
            cratePreparedStatement.executeUpdate();
            ResultSet resultSet = readPreparedStatement.executeQuery();
            ArrayList<String> names = getNames(resultSet);
            assertThat(names.size()).isNotZero();
            deletePreparedStatement.executeUpdate();
            assertThat(resultSet.getFetchSize()).isZero();
        }
    }

    @Test
    @SneakyThrows
    void crudPreparedStatements_AutocommitDisabled_FinalStateIsPersisted() {
        Connection externalConnection = getConnection();
        Connection connection = getConnection();
        connection.setAutoCommit(false);
        try (
            PreparedStatement cratePreparedStatement = getCratePreparedStatement(connection);
            PreparedStatement readPreparedStatement = getReadPreparedStatement(externalConnection);
        ) {
            cratePreparedStatement.executeUpdate();
            ResultSet resultSet = readPreparedStatement.executeQuery();
            ArrayList<String> names = getNames(resultSet);
            assertThat(names.size()).isZero();
            connection.commit();
            resultSet = readPreparedStatement.executeQuery();
            names = getNames(resultSet);
            assertThat(names.size()).isOne();
        } finally {
            connection.setAutoCommit(true);
            getDeletePreparedStatement(connection).executeUpdate();
        }
    }

    private ArrayList<String> getNames(ResultSet resultSet) throws SQLException {
        ArrayList<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(2));
        }
        return names;
    }

    @Test
    @SneakyThrows
    void crudPreparedStatements_RolledBackToSavepoint_FinalStateIsPersisted() {
        Connection connection = getConnection();
        connection.setAutoCommit(false);
        try (
            PreparedStatement cratePreparedStatement = getCratePreparedStatement(connection);
            PreparedStatement readPreparedStatement = getReadPreparedStatement(connection);
            PreparedStatement deletePreparedStatement = getDeletePreparedStatement(connection);
        ) {
            cratePreparedStatement.executeUpdate();
            Savepoint savepoint = connection.setSavepoint();
            deletePreparedStatement.executeUpdate();
            connection.rollback(savepoint);
            ResultSet resultSet = readPreparedStatement.executeQuery();
            ArrayList<String> names = getNames(resultSet);
            assertThat(names.size()).isOne();
            connection.commit();
            resultSet = readPreparedStatement.executeQuery();
            names = getNames(resultSet);
            assertThat(names.size()).isOne();
        } finally {
            connection.setAutoCommit(true);
            getDeletePreparedStatement(connection).executeUpdate();
        }
    }

    @Test
    @SneakyThrows
    void twoTransactions_ConcurrentUpdate_PhantomReadDetectsAChange() {
        Connection connection = getConnection();
        Connection phantomConnection = getConnection();

        assertTrue(phantomConnection.getMetaData()
            .supportsTransactionIsolationLevel(TRANSACTION_REPEATABLE_READ));

        // Фактически повышает до более строгой READ_COMMITTED:
        phantomConnection.setTransactionIsolation(TRANSACTION_REPEATABLE_READ);
        try (
            PreparedStatement createPreparedStatement = getCratePreparedStatement(connection);

            PreparedStatement updatePreparedStatement = getUpdatePreparedStatement(
                connection,"His Name", "My Name");

            PreparedStatement phantomUpdatePreparedStatement = getUpdatePreparedStatement(
                phantomConnection, "His Phantom Name", "My Name");
        ) {
            // Создали запись:
            createPreparedStatement.executeUpdate();

            // Намереваемся обновить в фантомной транзакции:
            phantomConnection.setAutoCommit(false);
            phantomUpdatePreparedStatement.executeUpdate();

            // Обновление в первой транзакции обнаруживает конфликт фантомного изменения:
            assertThrows(PSQLException.class, () -> updatePreparedStatement.executeUpdate());

            // Фантомная транзакция завершается успешно, если никто не вмешивался:
            phantomConnection.commit();
        } finally {
            getDeletePreparedStatement(connection);
            getDeletePreparedStatement(phantomConnection);
        }
    }
}
