package ru.otus.stream_api;

import static org.junit.jupiter.api.Assertions.assertFalse;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;

class CommonTest {

    /**
     * Синтаксис зависит от технологии: jdbc:mysql://localhost:3306/myDb?user=user1&password=pass
     * jdbc:postgresql://localhost/myDb jdbc:hsqldb:mem:myDb
     */
    @Test
    @SneakyThrows
    void dbServer_Connecting_Connected() {
        try (Connection connection = DriverManager.getConnection(
            "jdbc:postgresql://localhost:5432/postgres",
            "postgres",
            "postgres");) {

            assertFalse(connection.isClosed());
        }
    }

    /**
     * Пример c реализацией Hikari.
     */
    @Test
    @SneakyThrows
    void hikariConnectionPool_Connecting_Connected() {
        Connection connection = getConnection();
        assertFalse(connection.isClosed());
    }

    @Test
    @SneakyThrows
    void statement_Connecting_Connected() {
        Statement statement = getConnection().createStatement();
        statement.executeQuery("SELECT * FROM my_table WHERE name = 'My Name'");
    }

    @Test
    @SneakyThrows
    void preparedStatement_Connecting_Connected() {
        PreparedStatement preparedStatement = getReadPreparedStatement();

        preparedStatement.executeQuery();
        assertFalse(preparedStatement.isClosed());
    }

    protected PreparedStatement getCratePreparedStatement() throws SQLException {
        PreparedStatement preparedStatement = getConnection().prepareStatement(
            "INSERT INTO my_table (name) VALUES ('My Name')");

        return preparedStatement;
    }

    protected PreparedStatement getCratePreparedStatement(Connection connection)
        throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
            "INSERT INTO my_table (name) VALUES ('My Name')");

        return preparedStatement;
    }

    protected PreparedStatement getUpdatePreparedStatement(Connection connection, String setValue,
        String whereValue) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
            String.format("UPDATE my_table SET name = '%s' WHERE name = '%s'", setValue, whereValue));

        return preparedStatement;
    }

    protected PreparedStatement getReadPreparedStatement() throws SQLException {
        PreparedStatement preparedStatement = getConnection().prepareStatement(
            "SELECT * FROM my_table WHERE name = ?");

        int parameterPosition = 1;
        preparedStatement.setString(parameterPosition, "My Name");
        return preparedStatement;
    }

    protected PreparedStatement getReadPreparedStatement(Connection connection)
        throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
            "SELECT * FROM my_table WHERE name = ?");

        int parameterPosition = 1;
        preparedStatement.setString(parameterPosition, "My Name");
        return preparedStatement;
    }

    protected PreparedStatement getDeletePreparedStatement() throws SQLException {
        PreparedStatement preparedStatement = getConnection().prepareStatement(
            "DELETE FROM my_table");

        return preparedStatement;
    }

    protected PreparedStatement getDeletePreparedStatement(Connection connection)
        throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
            "DELETE FROM my_table");

        return preparedStatement;
    }

    protected Connection getConnection() throws SQLException {
        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        configuration.setUsername("postgres");
        configuration.setPassword("postgres");
        HikariDataSource dataSource = new HikariDataSource(configuration);
        return dataSource.getConnection();
    }
}
