package ru.otus.stream_api;

import static java.sql.ResultSet.CONCUR_UPDATABLE;
import static java.sql.ResultSet.TYPE_SCROLL_INSENSITIVE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.postgresql.util.PSQLException;

class ResultSetTest extends CommonTest {

    @Test
    @SneakyThrows
    void resultSet_PositionIsBeforeFirst_ExceptionIsThrown() {
        ResultSet resultSet = getReadPreparedStatement().executeQuery();
        // ResultSet not positioned properly, perhaps you need to call next:
        assertThrows(PSQLException.class, () -> resultSet.getString(0));
    }

    @Test
    @SneakyThrows
    void resultSet_Fetched_DataIsPresent() {
        getCratePreparedStatement().executeUpdate();
        getCratePreparedStatement().executeUpdate();
        ResultSet resultSet = getReadPreparedStatement().executeQuery();
        ArrayList<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(2));
        }
        assertThat(names).hasSize(2);
        getDeletePreparedStatement().executeUpdate();
    }

    @Test
    @SneakyThrows
    void resultSet_RecordAdded_DataIsPresent() {
        getCratePreparedStatement().executeUpdate();
        getCratePreparedStatement().executeUpdate();
        ResultSet resultSet = getReadPreparedStatement().executeQuery();
        assertThrows(PSQLException.class, resultSet::moveToInsertRow);
        getDeletePreparedStatement().executeUpdate();

        PreparedStatement preparedStatement = getConnection().prepareStatement(
            "SELECT * FROM my_table WHERE name = ?",
            TYPE_SCROLL_INSENSITIVE,
            CONCUR_UPDATABLE);

        preparedStatement.setString(1, "My Name");
        ResultSet updatableResultSet = preparedStatement.executeQuery();
        updatableResultSet.moveToInsertRow();
        updatableResultSet.updateString(2, "My Inserted Value");
        updatableResultSet.insertRow();
        updatableResultSet.moveToCurrentRow();
        ArrayList<String> names = new ArrayList<>();
        while (updatableResultSet.next()) {
            // TODO: Пример с маппингом
            names.add(updatableResultSet.getString(2));
        }
        assertThat(names).hasSize(1);
        assertThat(names.get(0)).isEqualTo("My Inserted Value");
        getDeletePreparedStatement().executeUpdate();
    }

    @Test
    @SneakyThrows
    void connection_MetaDataFetched_DatabaseItemsArePresent() {
        DatabaseMetaData databaseMetaData = getConnection().getMetaData();
        ResultSet tablesResultSet = databaseMetaData.getTables(null, "public", "%", null);
        ArrayList<String> databaseItems = new ArrayList<>();
        while (tablesResultSet.next()) {
            databaseItems.add(
                tablesResultSet.getString(2)
                    + " - "
                    + tablesResultSet.getString(3));
        }
        assertThat(databaseItems).isNotEmpty();
    }

    @Test
    @SneakyThrows
    void resultSet_MetaDataFetched_DatabaseItemsArePresent() {
        getCratePreparedStatement().executeUpdate();
        ResultSet resultSet = getReadPreparedStatement().executeQuery();
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        int columnCount = resultSetMetaData.getColumnCount();
        assertThat(columnCount).isEqualTo(2);
        getDeletePreparedStatement().executeUpdate();
    }
}
